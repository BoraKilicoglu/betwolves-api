'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/guides/routing
|
*/

const Route = use('Route')

Route.get('/', ({ request }) => {
  return { greeting: 'Hello world in JSON' }
})

// Route.get('/login', ({ request, auth }) => {
//   return { greeting: params }
// })

Route.get('register', 'UserController.store')
Route.get('login', 'UserController.login')

Route.get('users', 'UserController.index')
Route.get('users/:id', 'UserController.show').middleware('auth')
Route.get('user', 'UserController.user')

Route.get('register/profile', 'ProfileController.store')
Route.get('profiles', 'ProfileController.index')

Route.get('fetch', 'MatchController.fetch')
Route.get('matches', 'MatchController.index')
Route.get('matches/search', 'MatchController.search')

Route.get('coupon/create', 'CouponController.create')
Route.get('coupon/index', 'CouponController.index')
Route.get('coupon/delete', 'CouponController.delete')
Route.get('coupon/find', 'CouponController.find')





