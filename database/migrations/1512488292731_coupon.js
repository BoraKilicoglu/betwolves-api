'use strict'

const Schema = use('Schema')

class CouponSchema extends Schema {
  up () {
    this.create('coupons', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.string('matches').notNullable()
      table.integer('ratio').notNullable()
      table.integer('gain').notNullable()
      table.integer('money').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('coupons')
  }
}

module.exports = CouponSchema
