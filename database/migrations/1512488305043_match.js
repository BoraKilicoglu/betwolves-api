'use strict'

const Schema = use('Schema')

class MatchSchema extends Schema {

  up () {
    this.create('matches', (table) => {
      table.increments()
      table.integer("identifier").notNullable().unique()
      table.integer("mbc")
      table.string("odds")
      table.string("code")
      table.string("country")
      table.string("homeTeam")
      table.string("awayTeam")
      table.string("leagueName")
      table.string("leagueCode")
      table.string("startDate")
      table.integer("date")
      table.string("stats")
      table.integer("ranking")
      table.timestamps()
    })
  }

  down () {
    this.drop('matches')
  }
}

module.exports = MatchSchema
