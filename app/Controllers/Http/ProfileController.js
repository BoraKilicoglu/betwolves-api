'use strict'

const Profile = use('App/Models/Profile')

class ProfileController {
	async store ({ request, response }) {
		const data = request.only(['name', 'surname', 'user_id'])
		await Profile.create(data)
		return response.redirect('/')
	}

    async index () {
		const profiles = await Profile.all()
		return profiles.toJSON()
	}
}

module.exports = ProfileController
