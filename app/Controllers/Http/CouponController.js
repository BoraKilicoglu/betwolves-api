'use strict'

const Coupon = use('App/Models/Coupon')
const Database = use('Database')

class CouponController {
  async index (request, response){
    return await Database.select('*').from('coupons')
  }

  async find (request, response){
    const data = request.request.only(['user_id'])
    return await Coupon
      .query()    
      .where('user_id', data.user_id)
  }

  async delete (request, response){
    const data = request.request.only(['id'])
    return await Coupon
      .query()    
      .where('id', data.id).delete()
  }

  async create (request, response){
    // console.log(request.request.raw())
    const data = request.request.only(['money', 'matches', 'gain', 'ratio', 'user_id' ])
    await Coupon.create(data)
  }
}

module.exports = CouponController
