'use strict'

const Database = use('Database')
const Match = use('App/Models/Match') 
const Stat = use('App/Models/Stat') 

const moment = use('moment-timezone')
const co = require('co')
const got = use('got')

class MatchController {

    async index (request, response){
        return await Database.select('*').from('matches').orderBy('date')
    }

    async search (request, response){
        return await Match.query().where(request.request.all()).orderBy('date').fetch()
        // return await Database.select('*').from('matches').find(request.request.url())
    }

    fetch ({request, response}) {
        got('https://www.tuttur.com/draw/events/type/football').then(response => {
            let raw = JSON.parse(response.body);   
            saveMatches(raw);
        }).catch(error => {
            return error
        });
        function saveMatches (data) {
            co(function * () {
                for (let i in data.events) {
                    if (data.events[i].odds) {
                        let identifier = data.events[i].identifier
                        let check = yield Match.query().where('identifier', identifier)
                        if (check.length === 0) {
                            let one = data.events[i].odds["F.1"].toString().split(".")[0];
                            let two = data.events[i].odds["F.2"].toString().split(".")[0];
                            let rank = one == two ? 2 : 3
                            yield Match.create({
                                mbc: data.events[i].mbc,
                                odds: JSON.stringify(data.events[i].odds),
                                code: data.events[i].code,
                                country: data.events[i].country,
                                homeTeam: data.events[i].homeTeamName,
                                awayTeam: data.events[i].awayTeamName,
                                leagueName: data.events[i].leagueName,
                                leagueCode: data.events[i].leagueCode,
                                startDate: moment.unix(data.events[i].startDate).tz("Europe/Istanbul").format("HH:mm"),
                                identifier: data.events[i].identifier,
                                date: data.events[i].startDate,
                                ranking: rank
                            })
                        }
                    }   
                }
                getStats(data);
            })
        }

        function getStats(data){
            co(function * () {
                for (let i in data.events) {
                    if (data.events[i].odds) {
                        got('https://www.tuttur.com/draw/event-components/event/'+data.events[i].identifier+'').then(response => {
                            let raw = JSON.parse(response.body);
                            setStats(raw);
                        }).catch(error => {
                            console.log(error);
                        });
                    }
                }
            })
        }

        function setStats(data){
            co(function * () {
                let identifier = data.odds.identifier
                let check = yield Stat.query().where('identifier', identifier)
                if (check.length) {
                    updateStat(data, identifier);
                } else {
                    newStat(data, identifier);
                }
            })
        }

        function newStat(data, id){
            co(function * () {
                if (data.stats !== null) {
                    setFilters(data, id);
                    yield Stat.create({
                        identifier: id,
                        stats: JSON.stringify(data.stats),   
                        odds: JSON.stringify(data.odds)   
                    })
                } else {
                    yield Stat.create({
                        identifier: id,
                        odds: JSON.stringify(data.odds)   
                    })
                }
            })
        }


        function updateStat(data){
            console.log("required update");
        }

        function setFilters(data, id){
            co(function * () {
                let one = data.odds.groups[0].odds[0].outcome;
                let two = data.odds.groups[0].odds[2].outcome;
                one = one.toString().split(".")[0];
                two = two.toString().split(".")[0];

                if (data.stats.homeTeamPointTable.length !== 0 && data.stats.awayTeamPointTable.length !== 0) {
                    if (data.stats.awayTeamPointTable.total[0]["a"] == 1 || data.stats.homeTeamPointTable.total[0]["a"] == 1){
                        yield Match.query().where('identifier', id).update({ ranking: 1 })
                        console.log("LİDER");
                    } else {
                        console.log("LİDER DEĞİL")
                    }
                }
                // }else if(one == two){
                //     yield Match.query().where('identifier', id).update({ ranking: 2 })
                // }else {
                //     yield Match.query().where('identifier', id).update({ ranking: 3 })
                // }
            })
        }

    }
}

module.exports = MatchController
