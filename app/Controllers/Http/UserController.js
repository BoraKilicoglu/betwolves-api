'use strict'

const User = use('App/Models/User')

class UserController {

	async index () {
    const users = await User
    .query()
    .with('profile')
    .fetch()
		return users
	}

  create ({ view }) {
    return view.render('user.create')
  }

  async store ({ auth, request }) {
    const data = request.only(['username', 'email', 'password', 'password_confirmation'])
    await User.create(data)
    return await auth.withRefreshToken().attempt(data.email, data.password);
	}
	
	async login ({ request, auth }) {
    const { username, email, password } = request.all()
    return await auth.withRefreshToken().attempt(email, password);
    // const user = await auth.getUser()
    // return Object.assign({}, token, { user: user });
	}

	async show ({ auth, params }) {
    if (auth.user.id !== Number(params.id)) {
      return 'You cannot see someone else\'s profile'
    }
    return auth.user
  }

  async user ({auth}) {
    return auth.getUser()
  }
		
}

module.exports = UserController
